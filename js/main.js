let buttons = [...document.body.querySelectorAll('button')];
let buttonsColor = [];
for (let i = 0; i < buttons.length; i++) {
    buttonsColor[i] = false;
    buttons[i].addEventListener('click', () => {
        buttonClick(buttons[i], buttonsColor[i]);
        function buttonClick(button, color) {
            if (color == false) {
                buttonsColor[i] = true;
                button.style.background = "blue";
            } else {
                buttonsColor[i] = false;
                button.style.background = 'black';
            }
        }
    });
}

window.addEventListener('keyup', function keyup(key) {
    changeColorOnClick(key, 'Enter', buttons[0], 0);
    changeColorOnClick(key, 'KeyS', buttons[1], 1);
    changeColorOnClick(key, 'KeyE', buttons[2], 2);
    changeColorOnClick(key, 'KeyO', buttons[3], 3);
    changeColorOnClick(key, 'KeyN', buttons[4], 4);
    changeColorOnClick(key, 'KeyL', buttons[5], 5);
    changeColorOnClick(key, 'KeyZ', buttons[6], 6);
})
function changeColorOnClick(key, key2, button, color) {
    if (key.code == key2) {
        if (buttonsColor[color] == false) {
            buttonsColor[color] = true;
            button.style.background = "blue";  
        } else {
            buttonsColor[color] = false;
            button.style.background = 'black';
        }
    }
}